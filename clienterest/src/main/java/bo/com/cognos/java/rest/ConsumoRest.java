package bo.com.cognos.java.rest;

import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class ConsumoRest {
	public static void main(String[] args) {
		Client client = ClientBuilder.newClient();
		// listar(client);
		// String valor = login(client);
		crear(client);
		
	}
	private static void listar(Client client) {
		String url = "http://localhost:8080/proyectors/rest/usuario";
		String token = login(client);
		System.out.println("Listado con token: " + token);
		Response r = client.target(url).request(MediaType.APPLICATION_JSON).
				// header("Authorization", "Bearer 70cef410-7406-45a5-b4f1-2ae674368a25")
				header("Authorization", "Bearer " + token)
				.get();
		if(r.getStatus() == 200) {
			System.out.println("Correcto");
			List<Usuario> usuarios = r.readEntity(new GenericType<List<Usuario>>() {});
			for(Usuario usuario: usuarios) {
				System.out.println("Login: " + usuario.getLogin());
			}
		}
		else {
			System.out.println("Error: " + r.getStatus() + ", ");
		}
	}
	private static String login(Client client) {
		String url = "http://localhost:8080/proyectors/rest/usuario/login";
		Usuario u = new Usuario();
		u.setLogin("ccc");
		u.setPassword("123");
		Response r = client.target(url).request(MediaType.TEXT_PLAIN)
				.post(Entity.entity(u, MediaType.APPLICATION_JSON));
		String token = "";
		if(r.getStatus() == 200) {
			System.out.println("Correcto");
			// String token = r.readEntity(entityType)
			token = r.readEntity(String.class);			
		}
		else {
			System.out.println("Error: " + r.getStatus() + ", ");
			throw new RuntimeException("Error: " + r.getStatus());
		}
		return token;
	}

	private static void crear(Client client) {
		String url = "http://localhost:8080/proyectors/rest/usuario";
		Usuario u = new Usuario();
		u.setLogin("asdfghjkl");
		u.setPassword("123");
		u.setHabilitado(false);
		String token = login(client);
		System.out.println("Token: " + token);
		Response r = client.target(url).request(MediaType.APPLICATION_JSON)
				.header("Authorization", "Bearer " + token)
				.post(Entity.entity(u, MediaType.APPLICATION_JSON));
		if(r.getStatus() == 200) {
			System.out.println("Correcto");
			Usuario usuarioCreado = r.readEntity(Usuario.class);
			System.out.println("Usuario creado: " + usuarioCreado.getLogin());
		}
		else {
			System.out.println("Error: " + r.getStatus() + ", ");
			throw new RuntimeException("Error: " + r.getStatus());
		}
	}
	
}
