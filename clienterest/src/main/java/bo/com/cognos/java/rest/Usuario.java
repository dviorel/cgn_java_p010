package bo.com.cognos.java.rest;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Usuario {
	private Date fechaAlta;
	private Date fechaModificacion;
	private Date fechaBaja;
	private String login;
	private String password;
	private boolean habilitado;
}
