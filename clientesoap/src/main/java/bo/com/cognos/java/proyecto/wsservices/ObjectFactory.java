
package bo.com.cognos.java.proyecto.wsservices;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the bo.com.cognos.java.proyecto.wsservices package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _BorrarUsuarioResponse_QNAME = new QName("http://wsservices.proyecto.java.cognos.com.bo/", "borrarUsuarioResponse");
    private final static QName _CrearUsuarioResponse_QNAME = new QName("http://wsservices.proyecto.java.cognos.com.bo/", "CrearUsuarioResponse");
    private final static QName _CrearUsuario_QNAME = new QName("http://wsservices.proyecto.java.cognos.com.bo/", "CrearUsuario");
    private final static QName _LoginResponse_QNAME = new QName("http://wsservices.proyecto.java.cognos.com.bo/", "loginResponse");
    private final static QName _BorrarUsuario_QNAME = new QName("http://wsservices.proyecto.java.cognos.com.bo/", "borrarUsuario");
    private final static QName _Login_QNAME = new QName("http://wsservices.proyecto.java.cognos.com.bo/", "login");
    private final static QName _ActualizarUsuarioResponse_QNAME = new QName("http://wsservices.proyecto.java.cognos.com.bo/", "actualizarUsuarioResponse");
    private final static QName _ListarUsuariosResponse_QNAME = new QName("http://wsservices.proyecto.java.cognos.com.bo/", "listarUsuariosResponse");
    private final static QName _ActualizarUsuario_QNAME = new QName("http://wsservices.proyecto.java.cognos.com.bo/", "actualizarUsuario");
    private final static QName _ProyectoException_QNAME = new QName("http://wsservices.proyecto.java.cognos.com.bo/", "ProyectoException");
    private final static QName _ListarUsuarios_QNAME = new QName("http://wsservices.proyecto.java.cognos.com.bo/", "listarUsuarios");
    private final static QName _Usuario_QNAME = new QName("http://wsservices.proyecto.java.cognos.com.bo/", "usuario");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: bo.com.cognos.java.proyecto.wsservices
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link LoginResponse }
     * 
     */
    public LoginResponse createLoginResponse() {
        return new LoginResponse();
    }

    /**
     * Create an instance of {@link CrearUsuarioResponse }
     * 
     */
    public CrearUsuarioResponse createCrearUsuarioResponse() {
        return new CrearUsuarioResponse();
    }

    /**
     * Create an instance of {@link BorrarUsuarioResponse }
     * 
     */
    public BorrarUsuarioResponse createBorrarUsuarioResponse() {
        return new BorrarUsuarioResponse();
    }

    /**
     * Create an instance of {@link CrearUsuario }
     * 
     */
    public CrearUsuario createCrearUsuario() {
        return new CrearUsuario();
    }

    /**
     * Create an instance of {@link ActualizarUsuarioResponse }
     * 
     */
    public ActualizarUsuarioResponse createActualizarUsuarioResponse() {
        return new ActualizarUsuarioResponse();
    }

    /**
     * Create an instance of {@link ListarUsuariosResponse }
     * 
     */
    public ListarUsuariosResponse createListarUsuariosResponse() {
        return new ListarUsuariosResponse();
    }

    /**
     * Create an instance of {@link ListarUsuarios }
     * 
     */
    public ListarUsuarios createListarUsuarios() {
        return new ListarUsuarios();
    }

    /**
     * Create an instance of {@link Usuario }
     * 
     */
    public Usuario createUsuario() {
        return new Usuario();
    }

    /**
     * Create an instance of {@link ActualizarUsuario }
     * 
     */
    public ActualizarUsuario createActualizarUsuario() {
        return new ActualizarUsuario();
    }

    /**
     * Create an instance of {@link ProyectoException }
     * 
     */
    public ProyectoException createProyectoException() {
        return new ProyectoException();
    }

    /**
     * Create an instance of {@link BorrarUsuario }
     * 
     */
    public BorrarUsuario createBorrarUsuario() {
        return new BorrarUsuario();
    }

    /**
     * Create an instance of {@link Login }
     * 
     */
    public Login createLogin() {
        return new Login();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BorrarUsuarioResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsservices.proyecto.java.cognos.com.bo/", name = "borrarUsuarioResponse")
    public JAXBElement<BorrarUsuarioResponse> createBorrarUsuarioResponse(BorrarUsuarioResponse value) {
        return new JAXBElement<BorrarUsuarioResponse>(_BorrarUsuarioResponse_QNAME, BorrarUsuarioResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CrearUsuarioResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsservices.proyecto.java.cognos.com.bo/", name = "CrearUsuarioResponse")
    public JAXBElement<CrearUsuarioResponse> createCrearUsuarioResponse(CrearUsuarioResponse value) {
        return new JAXBElement<CrearUsuarioResponse>(_CrearUsuarioResponse_QNAME, CrearUsuarioResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CrearUsuario }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsservices.proyecto.java.cognos.com.bo/", name = "CrearUsuario")
    public JAXBElement<CrearUsuario> createCrearUsuario(CrearUsuario value) {
        return new JAXBElement<CrearUsuario>(_CrearUsuario_QNAME, CrearUsuario.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoginResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsservices.proyecto.java.cognos.com.bo/", name = "loginResponse")
    public JAXBElement<LoginResponse> createLoginResponse(LoginResponse value) {
        return new JAXBElement<LoginResponse>(_LoginResponse_QNAME, LoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BorrarUsuario }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsservices.proyecto.java.cognos.com.bo/", name = "borrarUsuario")
    public JAXBElement<BorrarUsuario> createBorrarUsuario(BorrarUsuario value) {
        return new JAXBElement<BorrarUsuario>(_BorrarUsuario_QNAME, BorrarUsuario.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Login }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsservices.proyecto.java.cognos.com.bo/", name = "login")
    public JAXBElement<Login> createLogin(Login value) {
        return new JAXBElement<Login>(_Login_QNAME, Login.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActualizarUsuarioResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsservices.proyecto.java.cognos.com.bo/", name = "actualizarUsuarioResponse")
    public JAXBElement<ActualizarUsuarioResponse> createActualizarUsuarioResponse(ActualizarUsuarioResponse value) {
        return new JAXBElement<ActualizarUsuarioResponse>(_ActualizarUsuarioResponse_QNAME, ActualizarUsuarioResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarUsuariosResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsservices.proyecto.java.cognos.com.bo/", name = "listarUsuariosResponse")
    public JAXBElement<ListarUsuariosResponse> createListarUsuariosResponse(ListarUsuariosResponse value) {
        return new JAXBElement<ListarUsuariosResponse>(_ListarUsuariosResponse_QNAME, ListarUsuariosResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActualizarUsuario }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsservices.proyecto.java.cognos.com.bo/", name = "actualizarUsuario")
    public JAXBElement<ActualizarUsuario> createActualizarUsuario(ActualizarUsuario value) {
        return new JAXBElement<ActualizarUsuario>(_ActualizarUsuario_QNAME, ActualizarUsuario.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProyectoException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsservices.proyecto.java.cognos.com.bo/", name = "ProyectoException")
    public JAXBElement<ProyectoException> createProyectoException(ProyectoException value) {
        return new JAXBElement<ProyectoException>(_ProyectoException_QNAME, ProyectoException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarUsuarios }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsservices.proyecto.java.cognos.com.bo/", name = "listarUsuarios")
    public JAXBElement<ListarUsuarios> createListarUsuarios(ListarUsuarios value) {
        return new JAXBElement<ListarUsuarios>(_ListarUsuarios_QNAME, ListarUsuarios.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Usuario }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsservices.proyecto.java.cognos.com.bo/", name = "usuario")
    public JAXBElement<Usuario> createUsuario(Usuario value) {
        return new JAXBElement<Usuario>(_Usuario_QNAME, Usuario.class, null, value);
    }

}
