
package bo.com.cognos.java.proyecto.wsservices;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.10
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "ProyectoServiceService", targetNamespace = "http://wsservices.proyecto.java.cognos.com.bo/", wsdlLocation = "file:/C:/Users/ABE/eclipse-workspace-javap010/clientesoap/src/wsdl/proyectoservice.wsdl")
public class ProyectoServiceService
    extends Service
{

    private final static URL PROYECTOSERVICESERVICE_WSDL_LOCATION;
    private final static WebServiceException PROYECTOSERVICESERVICE_EXCEPTION;
    private final static QName PROYECTOSERVICESERVICE_QNAME = new QName("http://wsservices.proyecto.java.cognos.com.bo/", "ProyectoServiceService");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("file:/C:/Users/ABE/eclipse-workspace-javap010/clientesoap/src/wsdl/proyectoservice.wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        PROYECTOSERVICESERVICE_WSDL_LOCATION = url;
        PROYECTOSERVICESERVICE_EXCEPTION = e;
    }

    public ProyectoServiceService() {
        super(__getWsdlLocation(), PROYECTOSERVICESERVICE_QNAME);
    }

    public ProyectoServiceService(WebServiceFeature... features) {
        super(__getWsdlLocation(), PROYECTOSERVICESERVICE_QNAME, features);
    }

    public ProyectoServiceService(URL wsdlLocation) {
        super(wsdlLocation, PROYECTOSERVICESERVICE_QNAME);
    }

    public ProyectoServiceService(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, PROYECTOSERVICESERVICE_QNAME, features);
    }

    public ProyectoServiceService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public ProyectoServiceService(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns ProyectoService
     */
    @WebEndpoint(name = "ProyectoServicePort")
    public ProyectoService getProyectoServicePort() {
        return super.getPort(new QName("http://wsservices.proyecto.java.cognos.com.bo/", "ProyectoServicePort"), ProyectoService.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns ProyectoService
     */
    @WebEndpoint(name = "ProyectoServicePort")
    public ProyectoService getProyectoServicePort(WebServiceFeature... features) {
        return super.getPort(new QName("http://wsservices.proyecto.java.cognos.com.bo/", "ProyectoServicePort"), ProyectoService.class, features);
    }

    private static URL __getWsdlLocation() {
        if (PROYECTOSERVICESERVICE_EXCEPTION!= null) {
            throw PROYECTOSERVICESERVICE_EXCEPTION;
        }
        return PROYECTOSERVICESERVICE_WSDL_LOCATION;
    }

}
