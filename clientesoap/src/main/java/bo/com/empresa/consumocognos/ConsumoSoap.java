package bo.com.empresa.consumocognos;

import java.util.List;

import bo.com.cognos.java.proyecto.wsservices.ProyectoException_Exception;
import bo.com.cognos.java.proyecto.wsservices.ProyectoService;
import bo.com.cognos.java.proyecto.wsservices.ProyectoServiceService;
import bo.com.cognos.java.proyecto.wsservices.Usuario;

public class ConsumoSoap {
	public static void main(String[] args) throws ProyectoException_Exception {
		ProyectoServiceService service = new ProyectoServiceService();
		ProyectoService proxy = service.getProyectoServicePort();
		listar(proxy);
		// crear(proxy);
		// actualizar(proxy);
		// borrar(proxy);
	}
	private static void listar(ProyectoService proxy) throws ProyectoException_Exception {
		List<Usuario> usuarios = proxy.listarUsuarios("");
		for(Usuario usuario: usuarios) {
			System.out.println("Login: " + usuario.getLogin());
		}
	}
	private static void crear(ProyectoService proxy) throws ProyectoException_Exception {
		Usuario u = new Usuario();
		u.setLogin("plmokn");
		u.setPassword("123");
		u.setHabilitado(false);
		Usuario usuarioCreado = proxy.crearUsuario(u);
		System.out.println("Usurio creado: " + usuarioCreado.getId());
	}
	private static void actualizar(ProyectoService proxy) throws ProyectoException_Exception {
		Usuario u = new Usuario();
		u.setId(28);
		u.setLogin("plmokn");
		u.setPassword("123");
		u.setHabilitado(true);
		Usuario usuarioCreado = proxy.actualizarUsuario(u);
		System.out.println("Usurio actualizado: " + usuarioCreado.getId());
	}
	private static void borrar(ProyectoService proxy) throws ProyectoException_Exception {
		proxy.borrarUsuario(28);
		System.out.println("Borrado exitoso");
	}
	
}
