package bo.com.cognos.java.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class JDBCTest {

	public static void main(String[] args) {
		Connection connection;
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			String url = "jdbc:mariadb://localhost:3306/demo";
			String user = "root";
			String password = "adminadmin";
			connection = DriverManager.getConnection(url, user, password);
			System.out.println("Conexi�n exitosa!!!");
			
			PreparedStatement ps = connection.prepareStatement("INSERT INTO PARAMETRO"
					+ " VALUES (?, ?)");
			ps.setString(1, "XXX");
			ps.setString(2, "YYYYY");
			int affectedRows = ps.executeUpdate();
			System.out.println("Se modificaron " + affectedRows + " filas.");
			Statement st = connection.createStatement();
			ResultSet rs = st.executeQuery("SELECT * from PARAMETRO"
					+ " -- WHERE CODIGO LIKE '%I%'");
			while(rs.next()) {
				System.out.println(rs.getString(1) + " - " + rs.getString(2));
			}
			connection.close();
		}
		catch(Exception e) {
			e.printStackTrace();
			System.out.println("Error: " + e.getMessage());
		}
	}
	
}
