package bo.com.cognos.java.jpa.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="PAR_PARAMETRO")
public class Parametro {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column(length=25, unique=true, updatable = false, name="C_CODIGO")
	private String codigo;
	@Column(length=100)
	private String valor;
	@Column(name="VALOR_NUMERICO", precision=10, scale=2)
	private BigDecimal valorNumerico;
	
	
	
	@Override
	public String toString() {
		return "Parametro [id=" + id + ", codigo=" + codigo + ", valor=" + valor + ", valorNumerico=" + valorNumerico
				+ "]";
	}
	public BigDecimal getValorNumerico() {
		return valorNumerico;
	}
	public void setValorNumerico(BigDecimal valorNumerico) {
		this.valorNumerico = valorNumerico;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	
	
	
	
}
