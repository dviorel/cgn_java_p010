package bo.com.cognos.java.jpa.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="PAR_PERSONA_ACTIVIDAD")
public class PersonaActividad {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	@JoinColumn(name="ID_PERSONA", nullable=false)
	@ManyToOne(fetch=FetchType.EAGER)
	private Persona persona;
	
	@Column(name="FECHA_ACTIVIDAD", nullable=false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaActividad;
	
	@Column(name="DESCRIPCION_ACTIVIDAD", columnDefinition="TEXT NOT NULL")
	private String descripcionActividad;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public Date getFechaActividad() {
		return fechaActividad;
	}

	public void setFechaActividad(Date fechaActividad) {
		this.fechaActividad = fechaActividad;
	}

	public String getDescripcionActividad() {
		return descripcionActividad;
	}

	public void setDescripcionActividad(String descripcionActividad) {
		this.descripcionActividad = descripcionActividad;
	}
	
}
