package bo.com.cognos.java.jpa.services;

import java.math.BigDecimal;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import bo.com.cognos.java.jpa.model.Parametro;

public class Test {

	public static void main(String[] args) {
		actualizar();
		// borrar();
	}
	
	private static void crear(Parametro p) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("TestPU");
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		em.persist(p);
		tx.commit();
	}
	
	private static void actualizar() {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("TestPU");
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		Parametro p = em.find(Parametro.class, 1);
		System.out.println("Par�metro antes: " + p);
		p.setValorNumerico(BigDecimal.ONE);
		p.setValor("Valor modificado 2");
		p.setCodigo("OTRO_CODIGO_3");
		em.persist(p);
		System.out.println("Par�metro despu�s: " + p);
		tx.commit();
	}
	
	private static void borrar() {
		EntityManager em = Persistence.createEntityManagerFactory("TestPU").createEntityManager();
		em.getTransaction().begin();		
		Parametro p = em.find(Parametro.class, 2);
		System.out.println("Par�metro antes: " + p);
		em.remove(p);
		em.getTransaction().commit();
	}
	
	
	
}
