package bo.com.cognos.java.jpa002;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name="PER_ACTIVIDAD")
@Getter
@Setter
@ToString(exclude="persona")
// @ToString
public class Actividad {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@JoinColumn(name = "PERSONA", nullable = false)
	@ManyToOne(fetch=FetchType.EAGER)
	private Persona persona;
	
	@Column(name="FECHA", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha;
	
	@Column(name="DESCRIPCION", length=180, nullable = false)
	private String descripcion;

}
