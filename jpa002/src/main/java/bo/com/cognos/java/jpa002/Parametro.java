package bo.com.cognos.java.jpa002;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="PAR4_PARAMETRO")
public class Parametro {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private Integer id;
	
	@Column(name="CODIGO", length = 25, updatable = false, unique = true)
	private String codigo;
	
	@Column(name="VALOR", length = 100, updatable = true)
	private String valor;
	
	@Column(name="VALOR_LARGO", columnDefinition="TEXT NOT NULL")
	private String valorLargo;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}
	
	

	public String getValorLargo() {
		return valorLargo;
	}

	public void setValorLargo(String valorLargo) {
		this.valorLargo = valorLargo;
	}

	@Override
	public String toString() {
		return "Parametro [id=" + id + ", codigo=" + codigo + ", valor=" + valor + ", valorLargo=" + valorLargo + "]";
	}

	
	
	
	
	
	
	
}
