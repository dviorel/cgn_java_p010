package bo.com.cognos.java.jpa002;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name="PER_PERSONA")
@Getter
@Setter
@ToString
public class Persona {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "NOMBRE", length = 30, nullable = false)
	private String nombre;
	
	@Column(name= "FECHA_NACIMIENTO", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date fechaNacimiento;
	
	@Column(name = "CI", length = 10, nullable = false, unique = true)
	private String ci;
	
	@Column(name = "SALARIO", precision = 8, scale = 2, nullable = false)
	private BigDecimal salario;
	
	@OneToMany(mappedBy="persona", fetch=FetchType.LAZY, 
			cascade=CascadeType.ALL)
	private Set<Actividad> actividades = new HashSet<Actividad>();
	

}
