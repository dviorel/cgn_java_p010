package bo.com.cognos.java.jpa002.utils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import bo.com.cognos.java.jpa002.Parametro;

public class TestParametro {

	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.
				createEntityManagerFactory("PUEjemplo");
		EntityManager em = emf.createEntityManager();
		crear(em);
		// actualizar(em);
		// borrar(em);
	}
	
	private static void crear(EntityManager em) {
		EntityTransaction et = em.getTransaction();
		et.begin();
		Parametro p = new Parametro();
		// p.setId(-2);
		p.setCodigo("COD2");
		p.setValor("VALOR2");
		p.setValorLargo("VALOR LARGO 2");
		em.persist(p);		
		System.out.println("Parámetro creado: " + p);
		et.commit();		
	}
	
	private static void actualizar(EntityManager em) {
		EntityTransaction et = em.getTransaction();
		et.begin();
		Parametro p = em.find(Parametro.class, 1);
		p.setCodigo("CODM3");
		p.setValor("VALORM3");
		em.persist(p);		
		et.commit();		
	}
	
	private static void borrar(EntityManager em) {
		EntityTransaction et = em.getTransaction();
		et.begin();
		Parametro p = em.find(Parametro.class, -1);
		em.remove(p);		
		et.commit();		
	}
	
}
