package bo.com.cognos.java.jpa002.utils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;

import bo.com.cognos.java.jpa002.Actividad;
import bo.com.cognos.java.jpa002.Parametro;
import bo.com.cognos.java.jpa002.Persona;

public class TestPersona {

	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.
				createEntityManagerFactory("PUEjemplo");
		EntityManager em = emf.createEntityManager();
		//crear(em);
		//StoredProcedureQuery spq;
		// spq.
		// leer(em);
		//query01(em);
		queryNativo(em);
	}
	
	private static void queryNativo(EntityManager em) {
		Query query = em.createNativeQuery("select * from PER_PERSONA "
				+ "WHERE ID > ?1");
		query.setParameter(1, 2);
		
		List<Object[]> datos = query.getResultList();
		for(Object[] dato: datos) {
			System.out.println("Col1: " + dato[0] + ", col2: " + dato[1] + ", col4: " + dato[3]);
		}
	}
	
	private static void query01(EntityManager em) {
		// Query  query = em.createQuery("select p FROM Persona p where p.nombre like 'C%'");
		/*Query  query = em.createQuery("select distinct a.persona FROM Actividad a "
				+ "where a.persona.nombre like 'C%'");*/
		int minimo = 10;
		int maximo = 30;
		
		Query  query = em.createQuery("select distinct p from Persona p "
				+ "left join fetch p.actividades "
				
			// 	+ "where (YEAR(current_date()) - YEAR(a.persona.fechaNacimiento)) between " + minimo + " and " + maximo);
			//	+ "where (YEAR(current_date()) - YEAR(p.fechaNacimiento)) "
			+ "where not p.actividades is empty ");
				// + "between :minimo and :maximo");
			//	+ "between ?1 and ?2");
		// query.setParameter("minimo", minimo);
		// query.setParameter(1, minimo);
		// query.setParameter("maximo", maximo);
		// query.setParameter(2, maximo);
		System.out.println("1111");
		List<Persona> personas = query.getResultList();
		System.out.println("2222");
		for(Persona persona: personas) {
			// System.out.println("3333");
			System.out.println("Persona: " + persona);
			// for(Actividad actividad: persona.getActividades()) {
			//	System.out.println("Actividad: " + actividad);
			//}
		}
		
	}
	
	
	private static void leer(EntityManager em) {
		Persona p = em.find(Persona.class, 2);
		System.out.println("UNO");
		if(!p.getActividades().isEmpty()) {
			p.getActividades().iterator().next().getDescripcion();
		}
		
		em.close();
		System.out.println("Persona: " + p);
	}
	
	private static void crear(EntityManager em) {
		EntityTransaction et = em.getTransaction();
		et.begin();
		Persona p = new Persona();
		p.setCi("12347");
		p.setFechaNacimiento(new Date());
		p.setNombre("CLaudio");
		p.setSalario(new BigDecimal("2060.98"));
		
		Actividad a1 = new Actividad();
		a1.setDescripcion("ACtividad 1");
		a1.setFecha(new Date());
		a1.setPersona(p);
		Actividad a2 = new Actividad();
		a2.setDescripcion("ACtividad 2");
		a2.setFecha(new Date());
		a2.setPersona(p);
		
		p.getActividades().add(a1);
		p.getActividades().add(a2);
		
		
		em.persist(p);		
		System.out.println("Persona creada: " + p);
		et.commit();		
	}
	
}
