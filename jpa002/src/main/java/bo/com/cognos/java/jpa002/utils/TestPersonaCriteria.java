package bo.com.cognos.java.jpa002.utils;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
// import org.eclipse.persistence.tools.profiler.PerformanceMonitor;

import bo.com.cognos.java.jpa002.Actividad;
import bo.com.cognos.java.jpa002.Persona;

public class TestPersonaCriteria {

	
	public static void mainJoin(String[] args) {
		EntityManagerFactory emf = Persistence.
				createEntityManagerFactory("PUEjemplo");
		EntityManager em = emf.createEntityManager();
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery cq = cb.createQuery(Actividad.class);
		Root actividad = cq.from(Actividad.class);
		Join<Actividad, Persona> persona = actividad.join("persona", 
				JoinType.RIGHT);
		cq.select(actividad);
		Query query = em.createQuery(cq);
		List<Actividad> actividades = query.getResultList();
		for(Actividad objActividad: actividades) {
			System.out.println("Actividad: " + objActividad);
		}
	}
	
	public static void mainCount(String[] args) {
		EntityManagerFactory emf = Persistence.
				createEntityManagerFactory("PUEjemplo");
		EntityManager em = emf.createEntityManager();
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery cq = cb.createQuery(Persona.class);
		Root persona = cq.from(Persona.class);
		cq.select(cb.count(persona));
		Query query = em.createQuery(cq);
		Long resultado = (Long)query.getSingleResult();
		System.out.println("Filas: " + resultado);
	}
	
	public static void mainUnDato(String[] args) {
		EntityManagerFactory emf = Persistence.
				createEntityManagerFactory("PUEjemplo");
		EntityManager em = emf.createEntityManager();
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Persona> cq = cb.createQuery(Persona.class);
		Root persona = cq.from(Persona.class);
		// cq.select(cb.max(persona.get("salario")));
		// cq.where(cb.greaterThan(persona.get("id"), 2));
		cq.select(cb.max(persona.get("salario"))).
				where(cb.greaterThan(persona.get("id"), 2));
		Query query = em.createQuery(cq);
		BigDecimal resultado = (BigDecimal)query.getSingleResult();
		System.out.println("Maximo: " + resultado);
	}
	
	public static void mainObjeto(String[] args) {
		EntityManagerFactory emf = Persistence.
				createEntityManagerFactory("PUEjemplo");
		EntityManager em = emf.createEntityManager();
		
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Persona> cq = cb.createQuery(Persona.class);
		Root persona = cq.from(Persona.class);
		// cq.where(cb.greaterThan(persona.get("salario"), 2000));
		// cq.where(cb.equal(persona.get("id"), 2));
		cq.where(cb.equal(persona.get("id"), cb.parameter(Integer.class, "id")));
		Query query = em.createQuery(cq);
		query.setParameter("id", 2);
		// List<Persona> personas = query.getResultList();
		Persona p = (Persona) query.getSingleResult();
		//for(Persona p: personas) {
			System.out.println("Persona: " + p);
		//}
	}
	
	public static void mainOrder(String[] args) {
		EntityManagerFactory emf = Persistence.
				createEntityManagerFactory("PUEjemplo");
		EntityManager em = emf.createEntityManager();
		
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Persona> cq = cb.createQuery(Persona.class);
		Root persona = cq.from(Persona.class);
		cq.where(cb.greaterThan(persona.get("salario"), 1000));
		cq.orderBy(cb.desc(persona.get("nombre")));
		Query query = em.createQuery(cq);
		List<Persona> personas = query.getResultList();
		for(Persona p: personas) {
			System.out.println("Persona: " + p);
		}
	}
	
	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.
				createEntityManagerFactory("PUEjemplo");
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaUpdate<Persona> cq = cb.createCriteriaUpdate(Persona.class);
		Root persona = cq.from(Persona.class);
		cq.where(cb.greaterThan(persona.get("id"), 2));
		cq.set(persona.get("salario"), new BigDecimal("999.99"));
		Query query = em.createQuery(cq);
		int filasAfectadas = query.executeUpdate();
		em.getTransaction().commit();
		System.out.println("Filas modificadas: " + filasAfectadas);
	}

	public static void mainDelete(String[] args) {
		EntityManagerFactory emf = Persistence.
				createEntityManagerFactory("PUEjemplo");
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaDelete<Persona> cq = cb.createCriteriaDelete(Persona.class);
		Root persona = cq.from(Persona.class);
		cq.where(cb.equal(persona.get("id"), 3));
		Query query = em.createQuery(cq);
		int filasAfectadas = query.executeUpdate();
		em.getTransaction().commit();
		System.out.println("Filas modificadas: " + filasAfectadas);
	}

}
