package bo.com.cognos.jsf.primerejercicio;

import java.util.Locale;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import bo.com.cognos.jsf.primerejercicio.model.Persona;

@ManagedBean
@ViewScoped
public class IndexBean {

	public String ingresar() {
		System.out.println("Usuario M: " + this.login);
		System.out.println("Password M: " + this.password);
		if(this.login.equals(this.password)) {	
			if(this.login.toUpperCase().startsWith("S")) {
				return "/super/usuariosuper?faces-redirect=true";
			}
			if(this.login.toUpperCase().startsWith("V")) {
				return "/anidado/vip/usuariovip?faces-redirect=true";
			}	
			return "usuario?faces-redirect=true";
			// return "uno?faces-redirect=true";
		}
		else {
			return "dos";
		}
	}
	
	private Long edad;
	private String login;
	private String password;
	
	public Long getEdad() {
		return edad;
	}
	public void setEdad(Long edad) {
		this.edad = edad;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public void cambiarAEspaniol() {
		
		// Persona p = new Persona();
		
		
		FacesContext.getCurrentInstance().getViewRoot().
		setLocale(new Locale("es"));
	}
	public void cambiarAIngles() {
		FacesContext.getCurrentInstance().getViewRoot().
		setLocale(new Locale("en"));		
	}
	public void cambiarAFrances() {
		FacesContext.getCurrentInstance().getViewRoot().
		setLocale(new Locale("fr"));
	}
	
	
	
}
