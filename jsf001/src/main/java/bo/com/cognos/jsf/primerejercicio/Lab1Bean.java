package bo.com.cognos.jsf.primerejercicio;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class Lab1Bean {

	private Integer num1;
	private Integer num2;
	private Integer num3;
	
	public Integer getNum1() {
		return num1;
	}
	public void setNum1(Integer num1) {
		this.num1 = num1;
	}
	public Integer getNum2() {
		return num2;
	}
	public void setNum2(Integer num2) {
		this.num2 = num2;
	}
	public Integer getNum3() {
		return num3;
	}
	
	public void sumar() {
		this.num3 = this.num1 + this.num2;
	}
	
	
	
}
