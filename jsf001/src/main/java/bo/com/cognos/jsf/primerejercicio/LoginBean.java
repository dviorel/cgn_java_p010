package bo.com.cognos.jsf.primerejercicio;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import lombok.Getter;
import lombok.Setter;

@ManagedBean
@ViewScoped
//@Getter
//@Setter
public class LoginBean {

	private String login;
	private String password;
	
	public String iniciarSesion() {
		if(this.login.equalsIgnoreCase(this.password)) {
			((HttpSession)FacesContext.getCurrentInstance().
					getExternalContext().getSession(true)).
				setAttribute("sesion", this.login);;
			
			return "/protegido/paginacontrolada?faces-redirect=true";
		}
		FacesContext.getCurrentInstance().addMessage("", 
			new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error al ingresar", 
				"Error al iniciar sesi�n con las credenciales entregadas"));
		return null;
	}

	public String cerrarSesion() {
		((HttpSession)FacesContext.getCurrentInstance().
				getExternalContext().getSession(true))
		.invalidate();
			
		return "/login.jsf?faces-redirect=true";
	}
	
	
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
