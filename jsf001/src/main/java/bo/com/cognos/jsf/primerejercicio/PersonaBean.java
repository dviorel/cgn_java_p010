package bo.com.cognos.jsf.primerejercicio;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;

import bo.com.cognos.jsf.primerejercicio.model.Persona;
import lombok.Getter;
import lombok.Setter;

@ManagedBean
@ViewScoped
@Getter
@Setter
public class PersonaBean {

	private List<Persona> personas;
	private Persona persona;
	private boolean editando;
	private boolean nuevo;
	
	@PostConstruct
	public void init() {
		// boolean editando = true;
		personas = new ArrayList<Persona>();
		personas.add(new Persona("Danny", 123, new Date()));
		personas.add(new Persona("Juan", 234, new Date()));
		personas.add(new Persona("Pedro", 345, new Date()));
		this.editando = false;
		this.nuevo = false;
	}
	
	public void eliminar() {
		this.personas.remove(this.persona);
		this.persona = null;
	}
	
	public void prepararNuevo() {
		this.persona = new Persona();
		//this.personas.add(persona);
		this.editando = true;
		this.nuevo = true;
	}
	
	public void guardar() {
		System.out.println("Nuevo es: " + this.nuevo + " con el objeto persona: " + this.persona);
		if(this.nuevo) {
			this.personas.add(persona);
		}
	}
	
}




