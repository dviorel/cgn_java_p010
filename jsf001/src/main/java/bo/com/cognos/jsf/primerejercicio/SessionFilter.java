package bo.com.cognos.jsf.primerejercicio;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SessionFilter implements Filter {

	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		
	}

	public void doFilter(ServletRequest request, 
			ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// Verifica si ya se escribi� la variable de sesi�n
		// "sesion"
		String sesion = (String)((HttpServletRequest)request).
				getSession().getAttribute("sesion");
		if(sesion != null) {
			// Autorizamos...
			chain.doFilter(request, response);
		}
		else {
			// Restringimos el acceso...
			String redireccion = ((HttpServletRequest)request).getContextPath()
					+ "/login.jsf";
			((HttpServletResponse)response).sendRedirect(redireccion);
		}
	}			

	public void destroy() {
		// TODO Auto-generated method stub
		
	}

}
