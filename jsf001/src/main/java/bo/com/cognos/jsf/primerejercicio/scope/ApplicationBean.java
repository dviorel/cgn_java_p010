package bo.com.cognos.jsf.primerejercicio.scope;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ApplicationScoped
public class ApplicationBean {

	@PostConstruct
	public void init() {
		System.out.println("Iniciando ApplicationScoped");
	}
	
	private String valor;
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	
	
	
}
