package bo.com.cognos.jsf.primerejercicio.scope;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;

@ManagedBean
@RequestScoped
public class RequestBean {

	@PostConstruct
	public void init() {
		System.out.println("Iniciando RequestScoped");
	}
	
	private String valor;
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	
	
	
}
