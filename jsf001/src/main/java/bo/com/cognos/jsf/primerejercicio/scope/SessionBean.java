package bo.com.cognos.jsf.primerejercicio.scope;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@ManagedBean
@SessionScoped
public class SessionBean {

	@PostConstruct
	public void init() {
		System.out.println("Iniciando SessionScoped");
	}
	
	private String valor;
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	
	public String getLogin() {
		return (String) ((HttpSession)FacesContext.
				getCurrentInstance().getExternalContext()
		.getSession(true)).getAttribute("sesion");
	}
	
}
