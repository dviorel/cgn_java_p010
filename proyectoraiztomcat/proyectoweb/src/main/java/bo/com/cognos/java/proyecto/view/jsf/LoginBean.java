package bo.com.cognos.java.proyecto.view.jsf;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import bo.com.cognos.java.proyecto.model.ProyectoException;
import bo.com.cognos.java.proyecto.model.Token;
import bo.com.cognos.java.proyecto.model.Usuario;
import bo.com.cognos.java.proyecto.services.TokenService;
import bo.com.cognos.java.proyecto.services.UsuarioService;
import bo.com.cognos.java.proyecto.services.XXXService;
import bo.com.cognos.java.proyecto.services.impl.UsuarioServiceImpl;
import lombok.Getter;
import lombok.Setter;

@ManagedBean
@ViewScoped
@Getter
@Setter
public class LoginBean extends XXXBean<Usuario, Integer> {

	@ManagedProperty("#{usuarioServiceImpl}")
	UsuarioService usuarioService;
	
	@ManagedProperty("#{tokenServiceImpl}")
	TokenService tokenService;
	
	public LoginBean() {
		super(Usuario.class);
	}

	@Override
	XXXService<Usuario, Integer> getService() {
		return usuarioService;
	}
	
	String login;
	String password;

	public String ingresar() {
		try {
			Usuario usuarioSesion = usuarioService.login(this.login, this.password);
			((HttpSession)(FacesContext.getCurrentInstance().getExternalContext().getSession(true))).setAttribute("usuariosesion", usuarioSesion);
			Token tk = tokenService.generarToken(usuarioSesion);
			log.info("TOken generado: " + tk.getToken());
			List<Token> tokens = tokenService.buscar("a");
			log.info("Encontr� " + tokens.size() + " tokens.");
			return "cliente?faces-redirect=true";
		}
		catch (ProyectoException e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMensajeUsuario(), e.getMensajeUsuario()));
			return null;
		}
	}
	
	
}
